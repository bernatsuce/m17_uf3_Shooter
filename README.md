Ha d’haver un jugador. El jugador ha d’implementar estructures de dades i tenir:
Vida (HP)
Recurs secundari (MP, PP, Stamina…)
Nivell (i experiència)
Objectes
Objectes d’equip que modifiquen les estadístiques
Consumibles
Estadístiques que afectin al combat
El jugador ha de conservar-se en canvis d’escenes (persistència)
Qualsevol altra que cregueu imprescindible

Han d’haver enemics. Els enemics han d’estar estructurats de forma òptima amb les estructures de dades necessàries per a un fàcil accés.
El codi ha d’estar implementat amb ScriptableObjects fent servir el paradigma de Data Driven Design. 
Ha d’haver una interfície de RPG, amb com a mínim un menú per a poder equipar al pj.
Ha d’haver persistència de dades
El projecte ha de seguir una planificació de projecte mitjançant les eines donades a classe (Diagrama de Gantt, Kanban)

Risketos Addicionals
-Pathfinding
-Fonaments d’IA en els enemics
